# Ansible role: labocbz.deploy_k3s_cluster

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Crombez-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)
![Tag: SSL/TLS](https://img.shields.io/badge/Tech-SSL/TLS-orange)
![Tag: Docker](https://img.shields.io/badge/Tech-Docker-orange)
![Tag: K3S](https://img.shields.io/badge/Tech-K3S-orange)
![Tag: Kubernetes](https://img.shields.io/badge/Tech-Kubernetes-orange)
![Tag: Portainer](https://img.shields.io/badge/Tech-Portainer-orange)

An Ansible playbook to install and configure a K3S Cluster on your hosts.

This playbook automates the deployment and configuration of K3S, a lightweight Kubernetes distribution, along with Docker, a platform for developing, shipping, and running applications in containers. The playbook also integrates with Portainer, a management tool for Docker and Kubernetes, and supports SSL/TLS configurations to ensure secure communication.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

In order to use this playbook, you have to know its required roles, their vars and their purppose. Further information [here](./roles/requirements.yml).

### Deployment diagramm

![Deployment-Diagram](./assets/Deployment-Diagram.svg)

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2024-01-09: First Init

* First init of this playbook with the bootstrap_playbook playbook by Lord Robin Crombez

### 2024-03-02: Fix and CI

* Added support for new CI base
* Edit all vars with __
* Tested and validated on Docker
* temp

### 2024-03-05: New CI, imported K3S

* K3S installed
* Apache2 installed
* Docker installed
* Tested

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch

### 2024-12-08: Global Refacto

* Refactor the whole playbook
* Add NSF support an packages installation
* Use latest version of the role
* Fit new guidelines

### 2025-01-01: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
